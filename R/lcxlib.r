#' Set column class types of a data frame
#' 
#' @param d A dataframe.
#' @param colClasses A character vector of column class types.
#' @return d with column classes as specified by colClasses
#' @examples
#' d = data.frame(list(x="1",y=1))
#' colClasses = c("numeric","character")
#' set_colClass(d, colClasses)
#' @export

set_colClass = function(d, colClasses) {
  colClasses = rep(colClasses, len=length(d))
  d[] = lapply(seq_along(d), function(i) switch(colClasses[i],
                                                 numeric=base::as.numeric(d[[i]]),
                                                 character=base::as.character(d[[i]]),
                                                 Date=base::as.Date(d[[i]], origin='1970-01-01'),
                                                 POSIXct=base::as.POSIXct(d[[i]], origin='1970-01-01'),
                                                 factor=base::as.factor(d[[i]]),
                                                 methods::as(d[[i]], colClasses[i]) ))
  return(d)
}
