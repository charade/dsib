#' Write Header of VCF File
#'
#' @param filename Name of output VCF file.
#' @examples
#' writeVCFHeader("example.vcf")
#' @export
writeVCFHeader=function(filename){
    cat("##fileformat=VCFv4.1\n",file=filename)
    today=Sys.Date()
    today=format(today,"%Y%m%d")
    cat("##filedate=",today,"\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=BKPTID,Number=.,Type=String,Description=\"ID of the assembled alternate allele in the assembly file\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=CIEND,Number=2,Type=Integer,Description=\"Confidence interval around END for imprecise variants\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=CIPOS,Number=2,Type=Integer,Description=\"Confidence interval around POS for imprecise variants\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=END,Number=1,Type=Integer,Description=\"End position of the variant described in this record\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=HOMLEN,Number=.,Type=Integer,Description=\"Length of base pair identical micro-homology at event breakpoints\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=HOMSEQ,Number=.,Type=String,Description=\"Sequence of base pair identical micro-homology at event breakpoints\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=IMPRECISE,Number=0,Type=Flag,Description=\"Imprecise structural variation\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=MEINFO,Number=4,Type=String,Description=\"Mobile element info of the form NAME,START,END,POLARITY\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=SVLEN,Number=.,Type=Integer,Description=\"Difference in length between REF and ALT alleles\">\n",sep="",file=filename,append=TRUE)
    cat("##INFO=<ID=SVTYPE,Number=1,Type=String,Description=\"Type of structural variant\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=DEL,Description=\"Deletion\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=DEL:ME:ALU,Description=\"Deletion of ALU element\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=DEL:ME:L1,Description=\"Deletion of L1 element\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=DUP,Description=\"Duplication\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=DUP:TANDEM,Description=\"Tandem Duplication\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=INS,Description=\"Insertion of novel sequence\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=INS:ME:ALU,Description=\"Insertion of ALU element\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=INS:ME:L1,Description=\"Insertion of L1 element\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=INV,Description=\"Inversion\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=CNV,Description=\"Copy number variable region\">\n",sep="",file=filename,append=TRUE)
    cat("##ALT=<ID=TRPINV,Description=\"Transposition or Inversion\">\n",sep="",file=filename,append=TRUE)
    cat("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n", file=filename,append=TRUE)
}

#' Write SV Contents to VCF File
#'
#' @param filename Name of output VCF file.
#' @param chr Chromosome
#' @param startpos Start position
#' @param ref Reference allele
#' @param alt Alternative allele
#' @param imprecise If the breakpoint is imprecise
#' @param endpos End position
#' @param svlen Length
#' @param cipos Confidence interval of start position
#' @param ciend Confidence interval of end position
#' @param id Id name
#' @param qual Quality score
#' @param filter If pass filter
#' @param purity Allele fraction
#' @param append If appending
#' @param somatic If somatic
#' @examples
#' writeVCFHeader("example.vcf")
#' @export
writeVCF=function(filename,chr,startpos,ref,alt,imprecise,endpos,svlen,cipos=NULL,ciend=NULL,id=NULL,qual=NULL,filter=NULL,purity=NULL,append=FALSE, somatic=TRUE){
    if(!append){
        # Write header.
        writeVCFHeader(filename)
    }
    nentries=length(chr)
    if(is.null(id)) id=rep(".",nentries)
    if(is.null(qual)) qual=rep("100",nentries)
    if(is.null(filter)) filter=rep("PASS",nentries)
    info=rep("",nentries)
    for(ei in 1:nentries){
        if(ei %%100==0) cat(ei,"\n")
        if(imprecise[ei]) info[ei] = "IMPRECISE;"
        info[ei] = paste(info[ei],"SVTYPE=",paste(alt[ei]),";",sep="")
        info[ei] = paste(info[ei],"END=",endpos[ei],";",sep="")
        info[ei] = paste(info[ei],"SVLEN=",svlen[ei],sep="")
        if(!is.null(cipos) && sum(is.na(cipos[ei,]))==0) info[ei] = paste(info[ei],";","CIPOS=",cipos[ei,1],",",cipos[ei,2],sep="")
        if(!is.null(ciend) && sum(is.na(ciend[ei,]))==0) info[ei] = paste(info[ei],";","CIEND=",ciend[ei,1],",",ciend[ei,2],sep="")    
        if(!is.null(purity)) info[ei] = paste(info[ei],";","PURITY=",purity[ei],sep="")    
        if(somatic) info[ei] = paste(info[ei],";","SOMATIC",sep="")    
    }

    for(ei in 1:nentries){
        if(ei %%100==0) cat(ei,"\n")
        cat(chr[ei],"\t",startpos[ei],"\t",id[ei],"\t",ref[ei],"\t<",paste(alt[ei]),">\t",qual[ei],"\t",filter[ei],"\t",info[ei],"\n",sep="",file=filename,append=TRUE)
    }
}

#' Write Breakend Mate to VCF File
#'
#' @param filename Name of output VCF file.
#' @param chr1 Chromosome of first
#' @param pos1 Start position of the first
#' @param ref1 Reference allele of the first
#' @param goRight1 Strand of the first
#' @param chr2 Chromosome of second
#' @param pos2 Start position of the second
#' @param ref2 Reference allele of the second
#' @param goRight2 Strand of the second
#' @param ins Inserted sequence
#' @param cipos1 Confidence interval of start position of the first
#' @param cipos2 Confidence interval of end position of the second
#' @param id Id name
#' @param qual Quality score
#' @param filter If pass filter
#' @param append If appending
#' @param somatic If somatic
#' @examples
#' writeOneSingleMateBndVCF("example.vcf","1","1000","A",TRUE,"2","2000","C",FALSE)
#' @export
writeOneSingleMateBndVCF=function(filename,chr1,pos1,ref1,goRight1,chr2,pos2,ref2,goRight2,ins=NULL,cipos1=NULL,cipos2=NULL,id=NULL,qual=NULL,filter=NULL,append=TRUE,somatic=TRUE){
    if(is.null(id)){
        id1 = "bnd_1"
        id2 = "bnd_2"
    } else {
        id1 = paste(id,"_1",sep="")
        id2 = paste(id,"_2",sep="")
    }
    
    if(is.null(ins)) ins=""
    
    if(goRight1 && goRight2){
        altstr1=paste("[",chr2,":",pos2,"[",ins,ref1,sep="")
        altstr2=paste("[",chr1,":",pos1,"[",ins,ref2,sep="")    
    }
    if(goRight1 && !goRight2){
        altstr1=paste("]",chr2,":",pos2,"]",ins,ref1,sep="")
        altstr2=paste(ref2,ins,"[",chr1,":",pos1,"[",sep="")
    }
    if(!goRight1 && goRight2){
        altstr1=paste(ref1,ins,"[",chr2,":",pos2,"[",sep="")
        altstr2=paste("]",chr1,":",pos1,"]",ins,ref2,sep="")
    }
    if(!goRight1 && !goRight2){
        altstr1=paste(ref1,ins,"]",chr2,":",pos2,"]",sep="")
        altstr2=paste(ref2,ins,"]",chr1,":",pos1,"]",sep="")
    }

    if(is.null(qual)){ qual1=".";qual2="."}
    if(is.null(filter)){ filter1="PASS";filter2="PASS"}

    info1=""
    if(!is.null(cipos1)) info1 = "IMPRECISE;"
    info1 = paste(info1,"SVTYPE=BND",sep="")
    info1 = paste(info1,";","MATEID=",id2,sep="")
    if(!is.null(cipos1)) info1 = paste(info1,";","CIPOS=",cipos1[1],",",cipos1[2],sep="")
    if(somatic) info1 = paste(info1,";","SOMATIC",sep="")    

    info2=""
    if(!is.null(cipos2)) info2 = "IMPRECISE;"
    info2 = paste(info2,"SVTYPE=BND",sep="")
    info2 = paste(info2,";","MATEID=",id2,sep="")
    if(!is.null(cipos2)) info2 = paste(info2,";","CIPOS=",cipos2[1],",",cipos2[2],sep="")
    if(somatic) info2 = paste(info2,";","SOMATIC",sep="")    

    cat(chr1,"\t",pos1,"\t",id1,"\t",ref1,"\t",altstr1,"\t",qual1,"\t",filter1,"\t",info1,"\n",sep="",file=filename,append=TRUE)
    cat(chr2,"\t",pos2,"\t",id2,"\t",ref2,"\t",altstr2,"\t",qual2,"\t",filter2,"\t",info2,"\n",sep="",file=filename,append=TRUE)
}

#' Read VCF file
#'
#' @param filename Name of output VCF file.
#' @param header If the VCF file has header
#' @return vcfTable A list which has CHR, START, END, LEN, ID, TYPE, PURITY fields
# need to add example data
#' @export
readVCF2=function(filename,header=TRUE){
    vcftab = utils::read.table(filename,sep="\t",header=header)
    svtype=paste(vcftab[,5])
    svend = rep(NA,nrow(vcftab))
    svlen = rep(NA,nrow(vcftab))
    purity = rep(NA,nrow(vcftab))
    for(i in 1:nrow(vcftab)){
        temp=strsplit(toString(vcftab[i,8]),";")[[1]]
        temp=strsplit(temp,"=")
        for(j in 1:length(temp)){
            if(temp[[j]][1] == "END") svend[i] = as.integer(temp[[j]][2])
            if(temp[[j]][1] == "SVTYPE") svtype[i] = temp[[j]][2]
            if(temp[[j]][1] =="SVLEN") svlen[i] = as.integer(temp[[j]][2])
            if(temp[[j]][1] =="PURITY") purity[i]=as.numeric(temp[[j]][2])
        }
    }
    list(chr=vcftab[,1],start=vcftab[,2],end=svend,len=svlen,id=vcftab[,3],type=svtype,purity=purity)
}


#' Find SVLEN from VCF's INFO field
#'
#' @param s The INFO field of a VCF entry
#' @return 0 always return zero
#' @examples
#' infoToSVLen("...;SVLEN=200;...;")
#' @export
infoToSVLen=function(s){
    temp=strsplit(s,split=";")[[1]]
    temp2=strsplit(temp,split="=")
    for(i in 1:length(temp2)){
        if(length(temp2[[i]])==2){
            if(temp2[[i]][1]=="SVLEN"){
                return(as.numeric(temp2[[i]][2]))
            }
        }
    }
    return(0)
}


#' drop call files based on n*MAD of number of calls
#'
#' @param vcf vcfList, a List of dataframes of VCF/BED 
#' @param n  Drop list item that its call number deviates at least n*MAD from the median
#' @return vcf vcfList, a List of dataframes of VCF/BED with dropped ones set to empty data.frame() 
#' @export 
dropNmad<-function(vcf, n){
  ncalls=sapply(vcf,FUN=nrow)
  ncalls_Nmad=stats::median(ncalls)+n*stats::mad(ncalls)
  for(i in seq_along(ncalls)){
    toDrop=if(nrow(vcf[[i]])>ncalls_Nmad) T else F 
    cat("dropNmad?",nrow(vcf[[i]]),ncalls_Nmad,toDrop,"\n")
    if(toDrop) { #need to output
      cat("too many fps, force ignore\n"); vcf[[i]]=data.frame() }
  }
  vcf
}

#' Read VCF file
#'
#' @param filename Name of output VCF file.
#' @return vcftable A data frame which has CHR, START, END, TYPE, METHOD, PURITY, TOTREADS fields
# need to add example data
#' @export
readVCF<-function(filename){
    tab=utils::read.table(filename,header=FALSE,sep="\t")
    temp=strsplit(paste(tab$V4),split=".",fixed=TRUE)
    method=rep("",nrow(tab)); 
    for(i in 1:length(temp)){
        method[i]=temp[[i]][length(temp[[i]])]
        if(substr(method[i],1,5)=="sclip") method[i]="sclip"
    }
    svfreq=rep(NA,nrow(tab))
    totreads=rep(NA,nrow(tab))
    for(i in 1:nrow(tab)){
        temp=strsplit(paste(tab$V9[i]),":",fixed=TRUE)
        temp=strsplit(temp[[1]][length(temp[[1]])],";",fixed=TRUE)
        temp=strsplit(temp[[1]],"=",fixed=TRUE)
        for(j in 1:length(temp)){
            if(temp[[j]][1]=="AA") svfreq[i] = as.numeric(temp[[j]][2])
            if(temp[[j]][1]=="DP") totreads[i] = as.numeric(temp[[j]][2])
        }
    }
    calls=data.frame(CHR=tab$V1,START=tab$V2,END=tab$V3,TYPE=tab$V6,METHOD=method,PURITY=svfreq,TOTREADS=totreads)
    calls
}

#' Overlap VCF entries among a list of VCFs
#'
#' @param vcf vcfList, a List of dataframes of VCF/BED, require CHR, START, END, LEN, TYPE, METHOD fileds 
#' @param LEN.RANGE Range of lengths entering calculation, default (0, Inf)
#' @param TYPE Type of calls entering calculation, default ALL
#' @param METHOD Type of calling methods entering calculation, default ALL
#' @param excludeXY If Chromosome x and y calls are excluded 
#' @param MAX.LEN Upper limit of lengths entering calculation, default 100000
#' @return overlaps A matrix that entry i,j is the number of overlapping events between (i,j)

overlapMatrix<-function(vcf, LEN.RANGE=c(0,Inf),TYPE="ALL",METHOD="ALL",excludeXY=TRUE,MAX.LEN=100000){
    cat("Constructing overlap matrix with TYPE=",TYPE,", METHOD=",METHOD, ", excludeXY=",excludeXY,", LEN.RANGE=",LEN.RANGE,"\n",sep="")
    irs=vector("list",length(vcf))
    irsAll=vector("list",length(vcf))
    for(i in 1:length(vcf)){
        tvcf=vcf[[i]]
        tvcf$CHR=paste(tvcf$CHR)
        if(excludeXY){
            throw=tvcf$CHR=="X" | tvcf$CHR=="Y" | tvcf$CHR=="23"
            tvcf=tvcf[!throw,]
        } 
        
        if(length(tvcf)>0){
            st=pmin(tvcf$START,tvcf$END)
            ed=pmax(tvcf$START,tvcf$END)
            tvcf$START=st
            tvcf$END=ed
            len=tvcf$END-tvcf$START
            matchType=rep(TRUE,length(len))
            matchMethod=rep(TRUE,length(len))
            if(TYPE != "ALL") matchType=(tvcf$TYPE==TYPE)
            if(METHOD!="ALL") matchMethod=(tvcf$METHOD==METHOD)
            sel=which(len>LEN.RANGE[1] & len<LEN.RANGE[2] & matchType & matchMethod)
            
            irs[[i]]=GenomicRanges::GRanges(seqnames=tvcf$CHR[sel],ranges=IRanges::IRanges(start=tvcf$START[sel]-500,end=tvcf$END[sel]+500))
            sel=which(len<MAX.LEN)  
            irsAll[[i]]=GenomicRanges::GRanges(seqnames=tvcf$CHR[sel],ranges=IRanges::IRanges(start=tvcf$START[sel]-500,end=tvcf$END[sel]+500))
        } else {
            irs[[i]]=GenomicRanges::GRanges()
            irsAll[[i]]=GenomicRanges::GRanges() 
        }
    }
    cat("Finished constructing Genomic Ranges.\n")
    overlaps=matrix(nrow=length(vcf), ncol=length(vcf))
    for(i in 1:length(vcf)){
        cat("i=",i,": ")
        for(j in 1:length(vcf)){
#            cat("i=",i,",j=",j,"\n")
            cat(".")
            overlaps[i,j]=sum(IRanges::countOverlaps(irs[[i]],irsAll[[j]])>0)
        }
        cat("done.\n")
    }
    overlaps
}

#' compute D-score of trios
#'
#' @param ov An overlap matrix, n by n
#' @param trios A trio index matrix, where each row specifies child, father and mother in order 
#' @param doplot If a histogram is plotted
#' @return dscore A list of computed dscore for each trio
computeDscores<-function(ov,trios,doplot=FALSE){
    dscore=rep(0,nrow(trios))
    for(i in 1:nrow(trios)){
        if(i %% 10000 == 0 ) cat(i," out of ",nrow(trios)," done.\n",sep="")
        dscore[i]=(ov[trios[i,1],trios[i,2]]-ov[trios[i,1],trios[i,3]])/ov[trios[i,1],trios[i,1]]
    }
    if(doplot) graphics::hist(dscore,col="cornflowerblue",100)
    list(dscores=dscore,dscore.mean=mean(dscore))
}

#' get all tiros from a family table
#'
#' @param sampleNames a vector of sampleNames entering the calculation
#' @param fam famTable, a family table at least have SeqCtr, FamID, Father, Mother, SUBJID fields
#' @return trios A trio index matrix, where each row specifies child, father and mother in order 
getDscoreTrios<-function(sampleNames,fam){
    mm=match(sampleNames,fam$SampleID)
    cat(sum(!is.na(mm)),"samples are found in family file.\n")
    fam=fam[mm,]

    samecenter=matrix(nrow=nrow(fam),ncol=nrow(fam),data=0)
    for(i in 1:nrow(fam)){
        sel=which(fam$SeqCtr == fam$SeqCtr[i])
        samecenter[i,sel]=1
    }

    fammat=matrix(nrow=nrow(fam),ncol=nrow(fam),data=0)
    ## 0=unrelated, 1=sib, 2=parent-child, 3=identity
    
    for(i in 1:nrow(fam)){
        sel=which(fam$FamID != fam$FamID[i])
        fammat[i,sel]=0
        sel=which(fam$Father==fam$Father[i] & fam$Mother==fam$Mother[i])
        fammat[i,sel]=1
        sel=which(fam$Father==fam$SUBJID[i] | fam$Mother==fam$SUBJID[i])
        fammat[i,sel]=2
        fammat[sel,i]=2
        fammat[i,i]=3
    }

    trios=data.frame(matrix(nrow=0,ncol=3))
    cat("Finding trios ...\n")
    for(i in 1:nrow(fam)){
        if(i %% 20==0) cat(i,"\n")
        sel=which((fammat[i,]==1 | fammat[i,]==2))
        if(length(sel)>0){
            outg=which(fammat[i,]==0)
            temp=cbind(i,rep(sel,length(outg)),rep(outg,each=length(sel)))
            trios=rbind(trios,temp)
        }
    }
    names(trios)=c("Center","Related","Outgroup")
    trios
}

#' Summarize vcf by call types
#' @param vcf vcfList a List of dataframes of VCF/BED, require CHR, START, END, LEN, TYPE, METHOD fileds 
#' @param sampleNames a vector of sampleNames entering the calculation
#' @param prefix Prefix name of output
#' @export 
summarizeVCFs<-function(vcf,sampleNames,prefix="Broad"){
    
    nTOTAL=rep(0,length(vcf))
    nDEL=rep(0,length(vcf))
    nINS=rep(0,length(vcf))
    nTRP=rep(0,length(vcf))
    nINV=rep(0,length(vcf))
    nTANDUP=rep(0,length(vcf))
    
    dels=GenomicRanges::GRanges()
    tandups=GenomicRanges::GRanges()
    insertions=GenomicRanges::GRanges()
    for(i in 1:length(vcf)){
        cat("Processing vcf ",i," out of ",length(vcf),"... \n",sep="")
        if(length(vcf[[i]])!=0){
            nTOTAL[i]=nrow(vcf[[i]])
            nDEL[i] = sum(vcf[[i]]$TYPE=="DEL",na.rm=TRUE)
            nINS[i] = sum(vcf[[i]]$TYPE=="INS" | vcf[[i]]$TYPE=="DOM.INS",na.rm=TRUE)
            nTRP[i] = sum(vcf[[i]]$TYPE=="TRP",na.rm=TRUE)
            nINV[i] = sum(vcf[[i]]$TYPE=="INV",na.rm=TRUE)
            nTANDUP[i] = sum(vcf[[i]]$TYPE=="TANDEM.DUP",na.rm=TRUE)
            sel=which(vcf[[i]]$TYPE=="DEL")
            if(length(sel)!=0) dels=c(dels,GenomicRanges::GRanges(seqnames=vcf[[i]]$CHR[sel],ranges=IRanges::IRanges(start=vcf[[i]]$START[sel],end=vcf[[i]]$END[sel])))
            sel=which(vcf[[i]]$TYPE=="TANDEM.DUP")
            if(length(sel)!=0) tandups=c(tandups,GenomicRanges::GRanges(seqnames=vcf[[i]]$CHR[sel],ranges=IRanges::IRanges(start=vcf[[i]]$START[sel],end=vcf[[i]]$END[sel])))
            sel=which(vcf[[i]]$TYPE=="INS")
            if(length(sel)!=0) insertions=c(insertions,GenomicRanges::GRanges(seqnames=vcf[[i]]$CHR[sel],ranges=IRanges::IRanges(start=vcf[[i]]$START[sel],end=vcf[[i]]$END[sel])))

        }
    }
    sumtab=data.frame(sampleNames,nTOTAL,nDEL,nINS,nTRP,nINV,nTANDUP)
    
    grDevices::png(paste(prefix,"_Totals.png",sep=""),height=600,width=900)
    graphics::par(mfrow=c(2,3))
    graphics::hist(nTOTAL,30,col="cornflowerblue")
    graphics::hist(nDEL,30,col="cornflowerblue")
    graphics::hist(nINS,30,col="cornflowerblue")
    graphics::hist(nTRP,30,col="cornflowerblue")
    graphics::hist(nINV,10,col="cornflowerblue")
    graphics::hist(nTANDUP,30,col="cornflowerblue")
    grDevices::dev.off()
    
    grDevices::png(paste(prefix,"_Pie.png",sep=""),height=600,width=600)
    slices=c(sum(nDEL),sum(nINS),sum(nTRP),sum(nINV),sum(nTANDUP))
    lbs=c("DEL","INS","TRP","INV","TANDUP")
    graphics::pie(slices,labels=lbs,main="Distribution of Called Event Types")
    grDevices::dev.off()

    grDevices::png(paste(prefix,"_width_del.png",sep=""),height=400,width=600)    
    wi=IRanges::width(dels)
    graphics::hist(pmin(wi,1000),100,col="cornflowerblue",main="Width of DEL",xlab="Bp")
    grDevices::dev.off()
    
}

#' Get family versus population sharing
#' @param vcfind VCF Index, which VCF is targeted
#' @param vcf VCFList, a list of all VCFs
#' @param fam famTable, a family table at least have SeqCtr, FamID, Father, Mother, SUBJID fields
#' @param sensmat, A data frame has sensitivity calculations based on length, has TYPE, SENS.HET, SENS.HOM, SIZE.LOWER and SIZE.UPPER fields
#' @return sharing Data Frame, with following fileds: overallFreq (fraction of population carry variant call), sibshare (fraction of sibs share variant call), sibcount (number of sibs), p0sib (error under null), p1sib (error under alt), L(D score)
#' @export
getFamilyVSPopOverlap2<-function(vcfind,vcf,fam,sensmat=NULL){
    #sink("DEBUG.txt", append=FALSE, split=FALSE)
    ovmat=matrix(nrow=nrow(vcf[[vcfind]]),ncol=length(vcf),data=0)
    # ovmat here is specific to vcf[[vcfind]], has dimensions (num of calls in vcf) x (num of vcfs)
    # i.e. each i-th row of ovmat is vcf[[vcfind]]'s i-th call 
    #      each j-th col of ovmat is how the calls over lap with j-th vcf
    chrs=unique(vcf[[vcfind]]$CHR)
    for(ch in chrs[!is.na(chrs)]){ #avoid CHR==NA
        cat("\tNow processing Chr ", ch, ": ",sep="")
        ptm=proc.time()
        sel=which(vcf[[vcfind]]$CHR==ch)
        ir1=IRanges::IRanges(start=vcf[[vcfind]]$START[sel],end=vcf[[vcfind]]$END[sel])
        for(i in seq_along(vcf)){
            sel2=which(vcf[[i]]$CHR==ch)
            ir2=IRanges::IRanges(start=vcf[[i]]$START[sel2],end=vcf[[i]]$END[sel2])
            ovmat[sel,i]=hasOverlap(ir1,ir2,MAX.OFFSET=200) #200bp allowance
        }
        elapsed=proc.time()-ptm
        cat("That took ",elapsed[3]," seconds.\n",sep="")
    } #ovmat[i,j] = T/F i-th variant in this sample's call overlaps with j-th sample's vcf
    
    TYPE=paste(vcf[[vcfind]]$TYPE)
    LEN=abs(vcf[[vcfind]]$END-vcf[[vcfind]]$START)
    
    sibsets=findSibs(fam)
    famsets=findFams(fam)
    overallCount=rowSums(ovmat)
    overallFreq=overallCount/ncol(ovmat)
    res=lapply(seq(nrow(ovmat)),FUN=function(x){ pSharing(ovmat[x,],sets=sibsets) }) #
    #res=apply(ovmat,1,pSharing,sets=sibsets)  #this breaks the program for some reason unknown.
    sibshare=rep(0,length(res)); sibcount=rep(0,length(res))
    for(i in 1:length(res)){ 
        sibshare[i]=res[[i]]$pshare
        sibcount[i]=res[[i]]$denom         #
    }
    
    p1sib=rep(NA,length(sibshare))  # p1 is the "p-value" assuming true.
    p0sib=rep(NA,length(sibshare))  # p0 is the "p-value" assuming false.
    for(i in which(!is.na(sibshare))){
        if(is.null(sensmat)){
            bHET=0.8; bHOM=1
        } else {
            ss=getSens(sensmat,TYPE[i],LEN[i])
            bHET=ss$bHET; bHOM=ss$bHOM
        }
        p1sib[i] = pSmallerThanObservedSharingIfTrue(sibshare[i],sibcount[i],overallFreq[i],bHET=bHET,bHOM=bHOM)
        p0sib[i] = pLargerThanObservedSharingIfFalse(sibshare[i],sibcount[i],overallFreq[i])
    }
    L=log(p1sib/p0sib)
	
    sharing=data.frame(overallFreq,sibshare,sibcount,p0sib,p1sib,L)
    sharing
}

#' get sensitvity of specific Type and Length
#' @param sensmat, A data frame has sensitivity calculations based on length, has TYPE, SENS.HET, SENS.HOM, SIZE.LOWER and SIZE.UPPER fields
#' @param ty, Type of variant call
#' @param len, Length of variant call
#' @return list, A list of bHET (sensitivity for heterozygous calls), bHOM (sensitvity for homozygous calls)

getSens<-function(sensmat,ty,len){
    rowi=which(sensmat$TYPE==ty & sensmat$SIZE.LOWER<=len & sensmat$SIZE.UPPER > len)
    if(length(rowi)==0){
        rowi=nrow(sensmat)
    } 
    list(bHET=sensmat$SENS.HET[rowi], bHOM=sensmat$SENS.HOM[rowi])
}

#' comput the probability that if the call is real and the Ped sharing is less than the observed Ped sharing  
#' @param obsp, observed Ped sharing (conditional probability)
#' @param n, is the total units for counting obsy=obsp*n 
#' @param f, is the population sharing
#' @param bHET, sensitivity for heterozygous calls
#' @param bHOM, sensitivity for homozygous calls
#' @return probability that if the call is real and the Ped sharing is less than the observed Ped sharing
#' @export
pSmallerThanObservedSharingIfTrue<-function(obsp,n,f,bHET=0.8,bHOM=1){
        pShare=computeFSIBfromF(f,bHET,bHOM)
        obsy=obsp*n
        stats::pbinom(obsy,n,pShare)
}

#' comput the probability that if the call is false and the Ped sharing is larger than the observed Pop sharing  
#' @param obsp, observed Ped sharing (conditional probability)
#' @param n, is the total units for counting obsy=obsp*n 
#' @param f, is the population sharing
#' @return probability that if the call is false and the Ped sharing is larger than the observed Pop sharing
pLargerThanObservedSharingIfFalse<-function(obsp,n,f){
        1-stats::pbinom(obsp*n,n,f)
}
    
#' Get family versus population sharing
#' @param chr Chromosome
#' @param chrst Chromosome Start
#' @param chred Chromosome End
#' @param gridsize Size of genome grid
#' @param vcf VCFList, a list of all VCFs
#' @param fam famTable, a family table at least have SeqCtr, FamID, Father, Mother, SUBJID fields
#' @param bHET, sensitivity for heterozygous calls
#' @param bHOM, sensitivity for homozygous calls
#' @param plotPngPrefix, what to plot
#' @param isSWAN, if the calls are made by SWAN
#' @return sharing Data Frame, with following fileds: sharestats, sibshare (fraction of sibs share variant call), famshare (fraction of fams share variant call), sibcount (number of sibs), famcount (number of sibs), overallFreq (overall pop freq), ranges (IRanges), chr (chromosome) 
#' @export
getFamilyVSPopOverlap<-function(chr,chrst,chred,gridsize,vcf,fam,bHET=NULL,bHOM=NULL,plotPngPrefix="sharing",isSWAN=TRUE){
    ir=IRanges::IRanges(start=seq(chrst,chred,gridsize),width=gridsize)
    cat("Getting Family VS Population Overlap for chr ",chr," containing ",length(ir), " regions.\n",sep="")
    called.irs=vector("list",length(vcf))
    for(i in 1:length(vcf)){
        tvcf=vcf[[i]]
        tvcf$CHR=paste(tvcf$CHR)
        if(length(tvcf)>0){
            st=pmin(tvcf$START,tvcf$END)
            ed=pmax(tvcf$START,tvcf$END)
            tvcf$START=st
            tvcf$END=ed
            len=tvcf$END-tvcf$START
            sel=which(tvcf$CHR==chr)
            called.irs[[i]]=IRanges::IRanges(start=tvcf$START[sel],end=tvcf$END[sel])
        } else {
            called.irs[[i]]=IRanges::IRanges()
        }
    }
    
    ovmat=matrix(nrow=length(ir),ncol=length(vcf))
    for(ci in 1:length(vcf)){
        ovmat[,ci] = IRanges::countOverlaps(ir,called.irs[[ci]])
    }
    ovmat=ovmat>0
    
    sibsets=findSibs(fam)
    famsets=findFams(fam)
    overallCount=rowSums(ovmat)  
    overallFreq=overallCount/nrow(fam)
    res=apply(ovmat,1,pSharing,sets=sibsets)
    sibshare=rep(0,length(res)); sibcount=rep(0,length(res))
    for(i in 1:length(res)){ 
        sibshare[i]=res[[i]]$pshare
        sibcount[i]=res[[i]]$denom
    }
    res=apply(ovmat,1,pSharing,sets=famsets)
    famshare=rep(0,length(res)); famcount=rep(0,length(res))
    for(i in 1:length(res)){ 
        famshare[i]=res[[i]]$pshare
        famcount[i]=res[[i]]$denom
    }
    
    lengths=c(IRanges::start(ir)[1],IRanges::width(ir))
    values=c(0,overallFreq)
    overallFreqRLE=S4Vectors::Rle(values, lengths)
    values=c(0,famshare)
    famshareRLE = S4Vectors::Rle(values, lengths)
    values=c(0,sibshare)
    sibshareRLE = S4Vectors::Rle(values, lengths)
    values=c(0,famcount)
    famcountRLE = S4Vectors::Rle(values,lengths)
    values=c(0,sibcount)
    sibcountRLE = S4Vectors::Rle(values,lengths)
    
    sharestats=vector("list",length(vcf))
    fRLEinRanges<-function(ranges,tRLE,f,...){
        res=rep(NA,length(ranges))
        for(i in 1:length(ranges)){
            res[i]=f(tRLE[ranges[i]],...)
        }
        res
    }
    
    p1sib=rep(NA,length(sibshare))  # p1 is the "p-value" assuming true.
    p0sib=rep(NA,length(sibshare))  # p0 is the "p-value" assuming false.
    for(i in which(!is.na(sibshare))){
        p1sib[i] = pSmallerThanObservedSharingIfTrue(sibshare[i],sibcount[i],overallFreq[i],bHET=bHET,bHOM=bHOM)
        p0sib[i] = pLargerThanObservedSharingIfFalse(sibshare[i],sibcount[i],overallFreq[i])
    }
    p1fam=rep(NA,length(famshare))  # p1 is the "p-value" assuming true.
    p0fam=rep(NA,length(famshare))  # p0 is the "p-value" assuming false.
    for(i in which(!is.na(famshare))){
        p1fam[i] = pSmallerThanObservedSharingIfTrue(famshare[i],famcount[i],overallFreq[i],bHET=bHET,bHOM=bHOM)
        p0fam[i] = pLargerThanObservedSharingIfFalse(famshare[i],famcount[i],overallFreq[i])
    }
    
    logpvalrat.sib=log(p1sib/p0sib)
    logpvalrat.fam=log(p1fam/p0fam)
    values=c(0,logpvalrat.sib)
    siblogpvalratRLE=S4Vectors::Rle(values,lengths)
    values=c(0,logpvalrat.fam)
    famlogpvalratRLE=S4Vectors::Rle(values,lengths)
    values=c(0,p0sib)
    p0sibRLE=S4Vectors::Rle(values,lengths)
    values=c(0,p1sib)
    p1sibRLE=S4Vectors::Rle(values,lengths)
    values=c(0,p0fam)
    p0famRLE=S4Vectors::Rle(values,lengths)
    values=c(0,p1fam)
    p1famRLE=S4Vectors::Rle(values,lengths)
    
    
    
    for(ci in 1:length(vcf)){
        cat("Computing sharing stats for vcf ",ci, " out of ",length(vcf),"... \n")
        tvcf=vcf[[ci]]
        tvcf$CHR=paste(tvcf$CHR)
        sel=which(tvcf$CHR==chr)
        ranges=called.irs[[ci]]
        meanfamshare=fRLEinRanges(ranges,famshareRLE,mean,na.rm=TRUE)
        meansibshare=fRLEinRanges(ranges,sibshareRLE,mean,na.rm=TRUE)
        meanoverallFreq=fRLEinRanges(ranges,overallFreqRLE,mean,na.rm=TRUE)
        meanlogfampop=fRLEinRanges(ranges,log2(famshareRLE/overallFreqRLE),mean,na.rm=TRUE)
        meanlogsibpop=fRLEinRanges(ranges,log2(sibshareRLE/overallFreqRLE),mean,na.rm=TRUE)
        minp0sib = fRLEinRanges(ranges,p0sibRLE,min, na.rm=TRUE)
        maxp1sib = fRLEinRanges(ranges,p1sibRLE,max, na.rm=TRUE)
        maxlogpvalratsib=fRLEinRanges(ranges,siblogpvalratRLE,max,na.rm=TRUE)
        minp0fam = fRLEinRanges(ranges,p0famRLE,min, na.rm=TRUE)
        maxp1fam = fRLEinRanges(ranges,p1famRLE,max, na.rm=TRUE)
        maxlogpvalratfam=fRLEinRanges(ranges,famlogpvalratRLE,max,na.rm=TRUE)
                
        if(isSWAN) {
            sharestats[[ci]]=data.frame(CHR=tvcf$CHR[sel],START=tvcf$START[sel],END=tvcf$END[sel],TYPE=tvcf$TYPE[sel],METHOD=tvcf$METHOD[sel],
                                    meanfamshare,meansibshare,meanoverallFreq,meanlogfampop,meanlogsibpop,
                                    minp0sib,maxp1sib,maxlogpvalratsib,minp0fam,maxp1fam,maxlogpvalratfam)
        } else {
            sharestats[[ci]]=data.frame(tvcf[sel,],
                                    meanfamshare,meansibshare,meanoverallFreq,meanlogfampop,meanlogsibpop,
                                    minp0sib,maxp1sib,maxlogpvalratsib,minp0fam,maxp1fam,maxlogpvalratfam)        
        }
     }
    
    
    if(!is.null(plotPngPrefix)){
        grDevices::png(paste(plotPngPrefix,1,"png",sep="."),height=900,width=400)
        sel=which(overallCount>0)
        graphics::par(mfrow=c(3,1))
        graphics::plot(overallCount[sel],sibshare[sel],xlab="Number of Carriers", ylab="Probability of Sib Sharing",xlim=c(0,155),ylim=c(0,1),cex.lab=1.5)
        graphics::abline(-1/155,1/155,col="red")
        graphics::grid()
        graphics::plot(overallCount[sel],famshare[sel],xlab="Number of Carriers", ylab="Probability of Family Sharing",cex.lab=1.5,xlim=c(0,155),ylim=c(0,1))
        graphics::grid()
        graphics::abline(-1/155,1/155,col="red")
        graphics::plot(log2(sibshare/overallFreq),log2(famshare/overallFreq), xlab="Log2(Sib/Population)",ylab="Log2(Family/Population",cex.lab=1.5)
        graphics::abline(0,1,col="red")
        graphics::grid()
        grDevices::dev.off()
        
        grDevices::png(paste(plotPngPrefix,2,"png",sep="."),height=900,width=1000)  
        graphics::par(mfrow=c(4,1))
        graphics::plot(IRanges::start(ir),overallCount, ylim=c(0,155),xlab="Position",ylab="Number of Carriers",main="Number of Carriers")
        graphics::grid()
        graphics::plot(IRanges::start(ir),sibshare,ylim=c(0,1),xlab="Position",ylab="Probabilty of Sib Sharing",main="Sib Sharing Ratio")
        graphics::grid()
        graphics::plot(IRanges::start(ir),log2(sibshare/overallFreq),xlab="Position",ylab="Log2 Sib/Population",main="Log2 Sib to Population Sharing Ratio")
        graphics::abline(0,0,col="red")
        graphics::grid()
        graphics::plot(IRanges::start(ir),log2(famshare/overallFreq),xlab="Position",ylab="Log2 Family/Population",main="Log2 Family to Population Sharing Ratio")
        graphics::abline(0,0,col="red")
        graphics::grid()
        grDevices::dev.off()
        
        grDevices::png(paste(plotPngPrefix,"logp","png",sep="."),height=900,width=1000)  
        graphics::par(mfrow=c(2,2))
        graphics::plot(log(p0sib),log(p1sib))
        graphics::abline(0,1,col="red")
        graphics::grid()
        graphics::plot(log(p0fam),log(p1fam))
        graphics::abline(0,1,col="red")
        graphics::grid()
        graphics::plot(logpvalrat.sib,logpvalrat.fam)
        graphics::abline(0,1,col="red")
        graphics::grid()
        graphics::hist(logpvalrat.sib,100,col="pink")
        grDevices::dev.off()
    }
    
    list(sharestats=sharestats,sibshare=sibshare,famshare=famshare,famcount=famcount,sibcount=sibcount,overallFreq=overallFreq,ranges=ir,chr=chr)
}

#' compute null sharing given popFreq and fam
#' @param popFreq population frquency of calls
#' @param fam famTable, a family table at least have SeqCtr, FamID, Father, Mother, SUBJID fields
#' @export
computeNullSharing<-function(popFreq,fam){
    sibsets=findSibs(fam)
    famsets=findFams(fam) 
    B=10000
    sibnull=matrix(nrow=B,ncol=length(popFreq))
    famnull=matrix(nrow=B,ncol=length(popFreq))
    for(pi in 1:length(popFreq)){
        cat(pi,"of",length(popFreq),"\n")
        p=popFreq[pi]
        n=nrow(fam)
        s=round(p*n)
        carriermat=matrix(nrow=B, ncol=n,data=0)
        for(b in 1:B){
            carriers=sample(n, s,replace=FALSE)
            carriermat[b,carriers]=1
        }
        sibnull[,pi]=apply(carriermat,1,pSharing,sets=sibsets)
        famnull[,pi]=apply(carriermat,1,pSharing,sets=famsets)
    }
    list(sibnull=sibnull,famnull=famnull,popFreq=popFreq,fam=fam)
}

#' find family of subject
#' @param fam famTable, a family table at least have SeqCtr, FamID, Father, Mother, SUBJID fields
#' @export
findFams<-function(fam){
    famsets=list("vector",length=nrow(fam))
    for(i in 1:nrow(fam)){                                                          
        famsets[[i]]=which(fam$FamID==fam$FamID[i])                                 #all rows that in Family
        famsets[[i]] = setdiff(famsets[[i]],i)                                      #except for i itself
    }
    famsets
}

#' find the sib sets based on family table
#' @param fam famTable, a family table at least have SeqCtr, FamID, Father, Mother, SUBJID fields
#' @return sibsets, a list where i-th vector contains all sibs of i excluding i
#' @export

findSibs<-function(fam){
    sibsets=list("vector",length=nrow(fam))
    for(i in 1:nrow(fam)){
        sibsets[[i]] = which(fam$Father==fam$Father[i] & fam$Mother==fam$Mother[i]) #all rows that share Father & Mother
        sibsets[[i]] = setdiff(sibsets[[i]],i)                                      #except for i itself
    }
    sibsets                                                                         #return a list
}

#' find pedigree/sib sharing
#' @param callvec A boolean vector of samples, True if carrying this variant call
#' @param sets Pedigree (Sib) sets of sample i, exluding i itself
#' @return list A list of fields, pshare (fraction of Pedigree/Sibs share the variant), denom (number of totol Peds/Sibs)
#' @export

pSharing=function(callvec,sets){
    sel=which(callvec==TRUE)                        #all samples have this call
    if(length(sel)==0){                             
        return(list(pshare=NA,denom=0))
    }
    sumshare=0
    denom=0
    for(i in 1:length(sel)){                        #for each sample i carryin this call
        thisset=sets[[sel[i]]]                      #sib sets of this sample i
        if(length(thisset)>0){
            sumshare=sumshare+sum(callvec[thisset]) #add this sib sets' call count to sib share
            denom=denom+length(thisset)             #add this sib sets' sib count to sib count
        }
    }
    list(pshare=sumshare/denom, denom=denom)
}  #when calculating pSharing we use all sib in all families, not only the sib of this sample 

#' check if two IRanges have overlaps 
#' @param ir1 IRanges set 1
#' @param ir2 IRanges set 2
#' @param MAX.OFFSET maximum offset of breakpoints
#' @return ovBoth a vector if i-th element of ir1 overlaps with any of ir2 
#' @export

hasOverlap<-function(ir1,ir2,MAX.OFFSET=200){
    st1=IRanges::IRanges(start=IRanges::start(ir1)-MAX.OFFSET, end=IRanges::start(ir1)+MAX.OFFSET)
    st2=IRanges::IRanges(start=IRanges::start(ir2),width=1)
    ovSt=IRanges::findOverlaps(st1,st2)
    ed1=IRanges::IRanges(start=IRanges::end(ir1)-MAX.OFFSET, end=IRanges::end(ir1)+MAX.OFFSET)
    ed2=IRanges::IRanges(start=IRanges::end(ir2),width=1)
    ovEd=IRanges::findOverlaps(ed1,ed2)
    
    inter=IRanges::intersect(ovSt,ovEd)
    temp=IRanges::as.matrix(inter)
    ovBoth=rep(FALSE,length(ir1))
    ovBoth[unique(temp[,1])]=TRUE
    ovBoth
}

#' plot shaded histogram
#' @param score vector of scores
#' @param shade which score to be shaded
#' @param horizontal plot horizontally
#' @param breaks plot breakpoints
#' @param shadeColor color of shade
#' @param nonshadeColor color of non shaded area
#' @param label label of shade
#' @param ... other parameters
#' @export

shadedHistogram<-function(score,shade,horizontal=TRUE,breaks=NULL,shadeColor="blue",nonshadeColor="gray",label="Score",...){
    if(!is.null(breaks)){
        score=threshold(score, min(breaks),max(breaks))
        hi=graphics::hist(score, breaks=breaks,plot=FALSE)
    } else {
        hi=graphics::hist(score, plot=FALSE)
        breaks=hi$breaks
    }
    

    
    if(horizontal){
        graphics::plot(rep(0,length(breaks)),breaks,xlab="Count",ylab=label,type="l",xlim=c(0,max(hi$counts)),...)
    } else {
        graphics::plot(breaks,rep(0,length(breaks)),ylab="Count",xlab=label,type="l",ylim=c(0,max(hi$counts)),...)    
    }
    shaded=rep(0,0)
    total=rep(0,0)
    for(bi in 1:(length(breaks)+1)){
        countTotal=sum(as.numeric(sel))
        sel=which(score>breaks[bi] & score<= breaks[bi+1])
        countTotal=length(sel)
        countShade=sum(shade[sel])
        if(horizontal){
           graphics::rect(0,breaks[bi],countTotal,breaks[bi+1],col=nonshadeColor)
           graphics::rect(0,breaks[bi],countShade,breaks[bi+1],col=shadeColor)
        } else {
           graphics::rect(breaks[bi],0,breaks[bi+1],countTotal,col=nonshadeColor)
           graphics::rect(breaks[bi],0,breaks[bi+1],countShade,col=shadeColor)        
        }
        shaded=c(shaded,countShade)
        total=c(total,countTotal)
    }
    list(shaded=shaded,total=total,breaks=breaks)
}

#' compute candidate f using sensitivity
#' @param p Pop frequency of calls
#' @param bHET sensitivity for heterozygous calls
#' @param bHOM sensitivity for homozygous calls
#' @param doplot if plot the computed candidate
#' @return list with following fields f(estimated Ped sharing), fSib1(). fSib2()
#' @export

computefs<-function(p,bHET,bHOM,doplot=TRUE){
    pHET=2*p*(1-p)
    pHOM=p^2
    pCALLSIBgHET=(bHET/2)*(1+p-p^2)+(bHOM/4)*(p+p^2)
    pCALLSIBgHOM=bHOM*(p+0.25*(1-p)^2)+(bHET/2)*(1-p^2)
    f=pHET*bHET+pHOM*bHOM     # observed call rate.
    pHETgCALL=(bHET*pHET)/f
    pHOMgCALL=1-pHETgCALL
    
    #fSib1=bHET/2 + f -f^2/(2*bHET)  # sib carrier rate from previous equation
    fSib1=bHET/2 + f/2 -f^2/(8*bHET)  # sib carrier rate from previous equation
    fSib2=pHETgCALL*pCALLSIBgHET+pHOMgCALL*pCALLSIBgHOM
    if(doplot){
        graphics::plot(f, fSib1,type="l",xlim=c(0,1),ylim=c(0,1), 
            xlab="P(Call)", ylab="P(Call | Call in Sib)",
            main=paste("HET: ",bHET,",      HOM: ",bHOM,sep=""))
        graphics::lines(f,fSib2,type="l",col="blue")
        graphics::grid()
        graphics::abline(0,1,col="gray")
        graphics::legend(x="bottomright",lty=c(1,1),col=c("black","blue"),legend=c("Approx","Truth"))
    }
    list(f=f,fSib1=fSib1,fSib2=fSib2)
}

#' compute Ped/Sib sharing based on Pop sharing 
#' @param f, population/Pop sharing
#' @param bHET, sensitivity for heterozygous calls
#' @param bHOM, sensitivity for homozygous calls
#' @param verbose, if show verbose message
#' @return Ped/Sib sharing based on Pop sharing 
#' @export

computeFSIBfromF<-function(f,bHET,bHOM,verbose=FALSE){
    if(f==0){
        return(0)
    }
    if(f>bHOM){
        fSib=f
        if(verbose) cat("Warning: f is larger than HOM sensitivity, setting fSib to ", fSib,".\n",sep="")
        return(fSib)
    }
    a=bHOM-2*bHET
    b=2*bHET
    c=-f
    if(b^2<4*a*c){
        if(verbose) cat("\t\tWarning: can not solve for p, resolve to HET approximation.\n")
        return(bHET/2 + f/2 -f^2/(8*bHET))
    }
    if(a==0){
        p1=f/(2*bHET)
        p2=p1
    } else {
        p1=(-b+sqrt(b^2-4*a*c))/(2*a)
        p2=(-b-sqrt(b^2-4*a*c))/(2*a)
    }
    fSib.1=0
    fSib.2=0
    if(p1>0 & p1<=1){
        fSib.1=computefs(p1,bHET,bHOM,doplot=FALSE)$fSib2
    }
    if(p2>0 & p2<=1){
        fSib.2=computefs(p2,bHET,bHOM,doplot=FALSE)$fSib2
    }
    fSib=0
    if(fSib.1>=f){
        fSib=fSib.1
    }
    if(fSib.2>= max(f,fSib.1)){
        fSib=fSib.2
    }
    if(fSib==0){
        if(verbose) cat("Warning: fSib.1=",fSib.1, ", fSib.2=",fSib.2,", f=",f,"\n")
        fSib=f        
    }
    fSib
}

#' thresholding a score
#' @param y a vector to be thresholded
#' @param miny bound of min
#' @param maxy bound of max
threshold<-function(y,miny,maxy){
    pmax(pmin(y,maxy),miny)
}

#' get element from a list of lists
#' @param ll a list of lists
#' @param elementNum collapsing the elementNum-th item
getElementFromListOfLists<-function(ll,elementNum){
    # li is a list, where each element is another list of K things.
    getElement<-function(lli,elementNum){
        lli[[elementNum]]
    }
    res=lapply(ll,getElement,elementNum)
    unlist(res)
}
