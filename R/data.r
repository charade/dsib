#' A list of VCF from real variant calling popultation
#'
#' @format A list with 232 items and each item is a dataframe with (CHR START END TYPE SIZE) fields:
"vcf"

#' A data frame of sib families from real variant calling popultaiton
#'
#' @format A data frame with 232 rows and 12 columns, which are (SUBJID SampleID FamID Father Mother Sex AD Age ADSP_WGS Subgroup SeqCtr SRR) fields 
"fam"

#' A data frame of sensitivities from real variant calling
#'
#' @format A data frame with 11 rows and 5 columns, which are (TYPE SIZE.LOWER SIZE.UPPER SENS.HET SENS.HOM) fields
"sensMat"
