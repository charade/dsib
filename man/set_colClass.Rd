% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/lcxlib.r
\name{set_colClass}
\alias{set_colClass}
\title{Set column class types of a data frame}
\usage{
set_colClass(d, colClasses)
}
\arguments{
\item{d}{A dataframe.}

\item{colClasses}{A character vector of column class types.}
}
\value{
d with column classes as specified by colClasses
}
\description{
Set column class types of a data frame
}
\examples{
d = data.frame(list(x="1",y=1))
colClasses = c("numeric","character")
set_colClass(d, colClasses)
}
