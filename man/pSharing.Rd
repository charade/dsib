% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/dsiblib.r
\name{pSharing}
\alias{pSharing}
\title{find pedigree/sib sharing}
\usage{
pSharing(callvec, sets)
}
\arguments{
\item{callvec}{A boolean vector of samples, True if carrying this variant call}

\item{sets}{Pedigree (Sib) sets of sample i, exluding i itself}
}
\value{
list A list of fields, pshare (fraction of Pedigree/Sibs share the variant), denom (number of totol Peds/Sibs)
}
\description{
find pedigree/sib sharing
}
