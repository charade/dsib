#!/usr/bin/env Rscript
global_ptm=proc.time()
source("adsplib.r")
setwd("~/ws/adsp/01_rawcalls")
FamilyFile="~/ws/adsp/01_rawcalls/ADSP_WGSfam_pilot.srr.txt"
famtab=read.table(FamilyFile,header=TRUE,sep="\t")
centers=c("Baylor","Broad","WashU")
#centers=c("Baylor")
Caller="HaplotypeCaller"
CallDir=paste("~/ws/adsp/01_rawcalls/",Caller,"/",sep="")
DataDir=paste("~/ws/adsp/03_dscore/PedigreeAnalysis/",sep="")

for(c in centers){
  vcf=list()
  fam=famtab[famtab$SeqCtr==c,]
  cat("c=",c,"n=",nrow(fam),"\n")
  for(i in seq_len(nrow(fam))){
    CallFile=paste(CallDir,paste(Caller,as.character(fam$SampleID[i]),"vcf",sep="."),sep="")
    cat("SampleID=",as.character(fam$SampleID[i]),"\n")
    cat("CallFile=",CallFile,"\n")
    tmp=read.table(CallFile)[,c(1,2,4,5)] #CHR(1), START(2), REF(3), ALT(4)
    #if len(REF) == 1 => INS; if len(REF) > 1  => DEL
    svtype=ifelse(nchar(as.character(tmp[,3]))>1,"DEL","INS") #ignore multi allele
    svsize=ifelse(nchar(as.character(tmp[,3]))>1,nchar(as.character(tmp[,3]))-1,0) #always assume INS size=1
    svend=as.integer(as.character(tmp[,2]))+svsize+1
    vcf[[i]]=as.data.frame(cbind(tmp[,1],as.integer(tmp[,2]),svend,svtype,svsize))
    colnames(vcf[[i]])=c("CHR","START","END","TYPE","SIZE")
  }

  vcf = dropNmad( vcf, 10 )

  RDataFile=paste(DataDir,paste(Caller,c,"raw.RData",sep="."),sep="")
  cat("RDataFile=",RDataFile,"\n")
  save(file=RDataFile,list=c("fam","vcf"))
}

global_elapsed=proc.time()-global_ptm
cat("convertHaplotypeCallerDone in", global_elapsed[3], "seconds\n")
