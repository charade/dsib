#!/usr/bin/env Rscript
global_ptm=proc.time()
source("adsplib.r")
setwd("~/ws/adsp/01_rawcalls")
SampleFile="~/ws/adsp/01_rawcalls/ADSP_WGSfam_pilot.srr.list"
srrtab=read.table(SampleFile,header=F,sep="\t")
centers=c("Baylor","Broad","WashU")
#centers=c("WashU")
Caller="Delly"
CallDir=paste("~/ws/adsp/01_rawcalls/",Caller,"/",sep="")
DataDir=paste("~/ws/adsp/03_dscore/PedigreeAnalysis/",sep="")

for(ci in seq_along(centers)){
  vcf=list()
  center=centers[ci]
  fam=famtab[famtab$SeqCtr==center,]  #pilot samples
  cat("c=",center,"n=",nrow(fam),"\n")
  print(head(fam))
  for(i in seq_len(nrow(fam))){
    cat("i=",i,"of n=",nrow(fam),"\n")
    srr=as.character(fam$SRR[i])
    fpat=paste("*.del.bed.gz$",sep="")
    fpath=paste(CallDir,center,"/",sep="")
    cat("fpat=",fpat,"\n")
    cat("fpath=",fpath,"\n")
    cat("srr=",srr,"\n")
    allFiles=list.files(path=fpath, pattern=fpat)
    CallFile=paste(fpath, allFiles[grep(srr,allFiles)][1], sep="")
    cat("CallFile=",CallFile,"\n")
    tmp = tryCatch( { v=read.table(CallFile); 
                           cat("nrow(v)=", nrow(v), "\n")
			   v=v[,1:5];
			   colnames(v)=c("CHR","START","END","TYPE","SIZE");
		           v },
	error=function(e) { data.frame() } ) #avoid empty call file issues
    vcf[[i]] = tmp
    cat("nrow(vcf[[i]])=", nrow(vcf[[i]]), "\n")
  }
  
  vcf = dropNmad( vcf, 10 )

  RDataFile=paste(DataDir, paste(Caller,center,"raw.RData",sep="."), sep="")
  cat("RDataFile=",RDataFile,"\n")
  save(file=RDataFile,list=c("fam","vcf"))
}
global_elapsed=proc.time()-global_ptm
cat("convertDellyDone in", global_elapsed[3], "seconds\n")
