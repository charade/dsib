#!/bin/bash

### Setup Environment Variables
export DSIB_BIN=`Rscript -e "cat(system.file('bin',package='dsib'))"`
export DSIB_EXTDATA="$DSIB_BIN/../extdata"

### Required Input, please change as you need
export DSIB_LOADDIR="$DSIB_BIN/../extdata/vcf"
export DSIB_OUTDIR="$DSIB_BIN/../extdata/out"
export DSIB_OPREFIX="$DSIB_BIN/../extdata/out/famBroad.ind"
export DSIB_FAMFILE="$DSIB_BIN/../extdata/famTable.txt"
export DSIB_FAMINDEXFILE="$DSIB_BIN/../extdata/famBroad.ind"
export DSIB_SENSFILE="$DSIB_BIN/../extdata/sensBroad.txt"

### Load VCF and generate RData, output in $DSIB_OPREFIX.dsib.RData
$DSIB_BIN/dsibLoadInput.R -o $DSIB_OPREFIX $DSIB_LOADDIR $DSIB_FAMFILE $DSIB_FAMINDEXFILE

### Compute D-score using $DSIB_OPREFIX.dsib.RData
$DSIB_BIN/dsibSharing.R -o $DSIB_OPREFIX.dsib $DSIB_OPREFIX.dsib.RData $DSIB_SENSFILE

### Export D-score using $DSIB_OPREFIX.dsib.post.RData
$DSIB_BIN/dsibExport.R -o $DSIB_OUTDIR $DSIB_OPREFIX.dsib.post.RData

