#!/usr/bin/env bash

#need to define bash array
mydate=$(LC_ALL=en_US.utf8 date +%Y%b%d)
freqArray=(00-02 02-05 05-10 10-20)
pctlArray=(00-02 02-05 05-10 10-20)
sizeArray=("0,30" "30,60,100,200,500,1000,5000,10000,100000")  
#for f in ${freqArray[*]}; do
#  for p in ${pctlArray[*]}; do
#    opt="-o ADSP-Pilot -f $f -p $p"
#    cmd="dsibPfreq.R $opt Breakdancer,CNVnator,Delly,HaplotypeCaller,Lumpy,Pindel,Scalpel,SWAN Baylor,Broad,WashU"
#    echo $cmd ">ADSP-Pilot.F$f.P$p.log.$mydate 2>&1 &"
#  done
#done

for file in `ls DscoreCalls/*.pfreq.bed`; do 
  for s in ${sizeArray[*]}; do
    cmd="dsibValidate.R -s $s -o $file.S$s -c Breakdancer,CNVnator,Delly,HaplotypeCaller,Lumpy,Pindel,Scalpel,SWAN $file"
    echo $cmd ">$file.S$s.log.$mydate 2>&1 &"
  done
done
