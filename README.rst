.. |Logo| image:: https://bytebucket.org/charade/dsib/wiki/images/logo.png
   :height: 40px
   :width: 50px

|Logo| Dsib - Statistical Variant Calling Quality Control with Multi Sibling Population
==================================================================================

.. contents:: Navigator


QUICK LINKS
-----------

`Examples <Example>`_

`Manuals <Manual>`_

`FAQ <FAQ>`_


INTRODUCTION
------------


  .. image:: https://bytebucket.org/charade/dsib/wiki/images/pipeline.png
   :alt: dsib_figure1.png
   :height: 320px
   :width: 400px
  
  Figure 1. The principle and workflow of Dsib.


IMPLEMENTATION
--------------

Currently, Dsib is implemented as a standard R Package.


INSTALL
-------------
in R:

.. code::

  options(download.file.method = "wget")  #this is only necessary if libcurl is not an option in R<3.2
  devtools::install_bitbucket("charade/dsib")

WIKI
--------

Dsib's https://bitbucket.org/charade/dsib/wiki page is a growing resource for manuals, FAQs and other information. This is a MUST read place before you actually using the Dsib tool. These documentations are also openly editable in reSructuredText format. You are more than welcome to contribute to this ongoing documentation.

CONTACTS
--------

Questions and comments shall be addressed to lixia at stanford dot edu.

CITATIONS
---------

The Dsib manuscript is under preparation.